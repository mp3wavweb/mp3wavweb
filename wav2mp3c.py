#!/usr/bin/env python3

import sys
import multiprocessing.connection

import mp3wavcfg

try:
    c = multiprocessing.connection.Client(mp3wavcfg.socketpath, 'AF_UNIX')
except FileNotFoundError:
    sys.stderr.write("no daemon running?\n")
    sys.exit(1)
c.send(("encode", "2009/12 - December/13"))
while True:
    msg = c.recv()
    if msg[0] != "progress":
        break
    print("%s%%" % msg[1])
print(msg)
