import os
import stat
import time
import io
import json
import urllib.parse
import sys
import multiprocessing.connection

sys.path.insert(0, os.path.dirname(__file__))

import mp3wavcfg

# Files modified less than this many seconds ago are hidden from view,
# to ensure we never serve partially written files.
MIN_AGE = 122

# How many jumps will the progress bar at most do?
FRACTIONS = 20

def quote(s):
    return urllib.parse.quote(s.encode("utf-8"))

# Taken from
# http://stackoverflow.com/questions/4579908/cross-platform-splitting-of-path-in-python
# but with debug removed since Python 3 does not support that print
# syntax (and debug was pointless anyhow).
def os_path_split_asunder(path):
    parts = []
    while True:
        newpath, tail = os.path.split(path)
        if newpath == path:
            assert not tail
            if path: parts.append(path)
            break
        parts.append(tail)
        path = newpath
    parts.reverse()
    return parts

class Response(Exception):
    def __init__(self, code, headers, body=""):
        self.__code = code
        self.__headers = headers
        self.__body = body

    def response(self, start_response):
        start_response(self.__code, self.__headers)
        return [self.__body]

class MethodNotAllowed(Response):
    def __init__(self, allowed):
        super().__init__("405 Method Not Allowed", [('Allow', allowed)])

class InternalError(Response):
    def __init__(self, reason):
        super().__init__("500 " + reason, [])

class MovedPermanently(Response):
    def __init__(self, location):
        super().__init__("301 Moved Permanently", [("Location", location)])

class NotFound(Response):
    def __init__(self):
        super().__init__("404 Not Found", [])

class JSONResponse(Response):
    def __init__(self, data):
        super().__init__("200 OK", [("Content-Type", "application/json")],
                         json.dumps(data))

def application(environ, start_response):
    try:
        return process_request(environ, start_response)
    except Response as r:
        return r.response(start_response)

def process_request(environ, start_response):
    if environ["REQUEST_METHOD"] not in ["GET", "POST"]:
        raise MethodNotAllowed('GET, POST')

    # Hack: Undo some duplicate utf-8 encoding we somehow end up with.
    path = environ['PATH_INFO'].encode("latin-1").decode("utf-8")

    # We want a trailing slash at the root.
    if path == "" and environ.get("SCRIPT_NAME", "") != "":
        raise MovedPermanently(environ["SCRIPT_NAME"] + "/")

    ph = PathHandler(path)
    status, real_file = ph.real_stat_and_file()

    if ph.extension() == ".mp3" and ph.real_stat(".wav") is not None:
        op_func = MP3_OPS.get(ph.operation())
        if op_func is not None:
            return op_func(environ, start_response, ph)

    if status is None:
        raise NotFound()
    elif stat.S_ISDIR(status.st_mode):
        return handle_directory(environ, start_response, ph, real_file)
    elif stat.S_ISREG(status.st_mode):
        return handle_file(environ, start_response, ph, real_file)
    else:
        raise NotFound()

def get_daemon_connection():
    try:
        return multiprocessing.connection.Client(mp3wavcfg.socketpath,
                                                 'AF_UNIX')
    except (FileNotFoundError, ConnectionRefusedError):
        pass

    try:
        os.remove(mp3wavcfg.socketpath)
    except FileNotFoundError:
        pass

    pid = os.spawnl(os.P_NOWAIT,
                    os.path.join(os.path.dirname(__file__),
                                 "wav2mp3d.py"),
                    "wav2mp3d.py")
    for i in range(200):
        time.sleep(0.05)
        try:
            return multiprocessing.connection.Client(mp3wavcfg.socketpath,
                                                     'AF_UNIX')
        except (FileNotFoundError, ConnectionRefusedError):
            pass
    raise InternalError("failed to start wav2mp3d")


def convert(environ, start_response, ph):
    if len(ph.op_args()) > 0:
        raise NotFound()

    if environ["REQUEST_METHOD"] == "GET":
        start_response("200 OK",
                       [('Content-Type', 'text/html;charset=utf-8')])
        return ["<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n"
                "  \"http://www.w3.org/TR/html4/strict.dtd\">\n"
                "<html><head><title>Request conversion</title></head>\n"
                "<body><form method=\"POST\" action=\"convert\">\n"
                "<button>Confirm conversion to mp3</button>\n"
                "</form></body></html>"]

    if environ["REQUEST_METHOD"] != "POST":
        raise MethodNotAllowed('GET, POST')

    c = get_daemon_connection()
    c.send(("encode", ph.rel_base()))
    while True:
        msg = c.recv()
        if msg[0] not in ["progress", "enqueued"]:
            break

    raise MovedPermanently("..")

def convert_start(environ, start_response, ph):
    if len(ph.op_args()) > 0:
        raise NotFound()

    if environ["REQUEST_METHOD"] != "POST":
        raise MethodNotAllowed('POST')

    c = get_daemon_connection()
    c.send(("encode", ph.rel_base()))
    msg = c.recv()
    if msg[0] == "enqueued":
        raise JSONResponse({"status":"enqueued",
                            "progress":0,
                            "workload":msg[2]})
    elif msg[0] == "enofile":
        raise NotFound()
    elif msg[0] == "already-there":
        raise JSONResponse({"status": "done",
                            "size": sizeof_fmt(ph.real_stat().st_size),
                            "link": quote(ph.basename() + ".mp3")})
    else:
        raise InternalError("bad response %s to encode request" % (msg[0], ))

def conversion_progress(environ, start_response, ph):
    if len(ph.op_args()) != 2:
        raise NotFound()

    seen_progress = int(ph.op_args()[0])
    original_workload = int(ph.op_args()[1])
    seen_fractions = (FRACTIONS * seen_progress) // original_workload

    if environ["REQUEST_METHOD"] != "POST":
        raise MethodNotAllowed('POST')

    time.sleep(0.2)

    c = get_daemon_connection()
    c.send(("subscribe", ph.rel_base()))
    while True:
        try:
            msg = c.recv()
        except EOFError:
            raise InternalError("wav2mp3d died\n")
        if msg[0] == "done":
            raise JSONResponse({"status": "done",
                                "size": sizeof_fmt(ph.real_stat().st_size),
                                "link": quote(ph.basename() + ".mp3")})
        elif msg[0] == "progress":
            fn = msg[1]
            progress = msg[2]
            workload = msg[3]
            adjusted_progress = progress + (original_workload - workload)
            adjusted_fractions = (FRACTIONS * adjusted_progress) // original_workload
            if adjusted_fractions != seen_fractions:
                raise JSONResponse({"status": "progress",
                                    "progress": adjusted_progress,
                                    "workload": original_workload})
        else:
            raise InternalError("Unexpected response %s\n" % (msg[0],))

MP3_OPS = {
    'convert': convert,
    'convert_start': convert_start,
    'convert_cont': conversion_progress,
}

class PathHandler(object):
    def __init__(self, path):
        self.__parts = os_path_split_asunder(path)
        if len(self.__parts) > 0 and self.__parts[0] == "/":
            self.__parts = self.__parts[1:]

        (self.__dirparts, self.__basename,
         self.__extension, self.__ops) = self.__split_path(self.__parts)

    def basename(self):
        return self.__basename

    def __split_path(self, parts):
        pos = len(parts) - 1
        while pos >= 0:
            fn, ext = os.path.splitext(parts[pos])
            if ext in [".mp3", ".wav", ".js"]:
                return parts[:pos], fn, ext, parts[pos+1:]
            pos -= 1
        if len(parts) == 0:
            return [], "", "", None
        else:
            return parts[:-1], parts[-1], "", None

    def path_parts(self, ext_override=None):
        fn =  self.__basename + self.extension(ext_override)
        return self.__dirparts + [fn]

    def real_stat_and_file(self, ext_override=None):
        path_parts = self.path_parts(ext_override)
        if self.extension(ext_override) == ".js":
            real_file = os.path.join(os.path.dirname(__file__), *path_parts)
        else:
            real_file = os.path.join(mp3wavcfg.BASE, *path_parts)

        try:
            return os.stat(real_file.encode("utf-8")), real_file
        except FileNotFoundError:
            pass

        if self.extension(ext_override) == ".mp3":
            mp3file = os.path.join(mp3wavcfg.BASE, "mp3-cache", *path_parts)
            try:
                return os.stat(mp3file.encode("utf-8")), mp3file
            except FileNotFoundError:
                pass

        return None, None

    def real_stat(self, ext_override=None):
        return self.real_stat_and_file(ext_override)[0]

    def real_file(self, ext_override=None):
        return self.real_stat_and_file(ext_override)[1]

    def extension(self, ext_override=None):
        if ext_override is None:
            return self.__extension
        else:
            return ext_override

    def operation(self):
        if self.__ops is None:
            return None
        if len(self.__ops) <1:
            return None
        return self.__ops[0]

    def op_args(self):
        assert self.__ops is not None
        assert len(self.__ops) > 0
        return self.__ops[1:]

    def rel_base(self):
        return os.path.join(*(self.__dirparts + [self.__basename]))

    def descendant(self, filename):
        return PathHandler(os.path.join(self.rel_base() + self.__extension,
                                        filename))

def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB']:
        if num < 1024.0 and num > -1024.0:
            return "%.1f %s" % (num, x)
        num /= 1024.0
    return "%.1f %s" % (num, 'TB')

def handle_directory(environ, start_response, ph, real_dir):
    wavs = {}
    mp3s = {}
    too_new = {}
    dirs = set()

    now = time.time()

    for fn in os.listdir(real_dir.encode("utf-8")):

        fn = fn.decode("utf-8")

        # Don't expose the cache directory directly.
        if fn == "mp3-cache":
            continue

        realfile = os.path.join(real_dir, fn)
        try:
            status = os.stat(realfile.encode("utf-8"))
        except os.error:
            continue

        root, ext = os.path.splitext(fn)

        if ext == ".wav":
            if now - status.st_mtime >= MIN_AGE:
                wavs[root] = status
            else:
                too_new[root] = status
        elif ext == ".mp3":
            mp3s[root] = status
        elif stat.S_ISDIR(status.st_mode):
            dirs.add(fn)

    res = io.StringIO()
    res.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\"\n")
    res.write("  \"http://www.w3.org/TR/html4/strict.dtd\">\n")
    res.write("<html>")
    res.write("<head>")
    res.write("<script src='")
    res.write(environ["SCRIPT_NAME"])
    res.write("/mp3wavweb/js/jquery-2.1.0.min.js'></script>")
    res.write("<script src='")
    res.write(environ["SCRIPT_NAME"])
    res.write("/mp3wavweb/js/mp3wavweb.js'></script>")
    res.write("</head>")
    res.write("<body>")
    parts = ph.path_parts()
    if len(parts) > 1:
        res.write("\n<a href=\"%s\">(root)</a>" % ("../" * (len(parts) - 1)))
        for n, part in enumerate(parts[:-1]):
            res.write("\n / <a href=\"%s\">%s</a>" % (
                "../" * (len(parts) - n - 2), part))
    res.write("<ul>")
    if len(parts) > 1:
        res.write("<li><a href=\"..\">(up one level)</a></li>\n")
    for d in sorted(dirs):
        res.write("<li>")
        res.write("<a href=\"%s/\">%s</a>" % (quote(d), d))
        res.write("</li>\n")
    for w in sorted(too_new):
        res.write("<li>")
        res.write(time.strftime("%Y-%m-%d %H:%M:%S ",
                                time.localtime(too_new[w].st_mtime)))
        res.write(w)
        res.write(" (Still being written? Please retry soon.)")
        res.write("</li>")
    for w in sorted(wavs):
        res.write("<li>")
        res.write(time.strftime("%Y-%m-%d %H:%M:%S",
                                time.localtime(wavs[w].st_mtime)))
        res.write(" <a href=\"%s.wav\">%s.wav</a>" % (
            quote(w), w))
        res.write(" (" + sizeof_fmt(wavs[w].st_size) + ")")
        if w in mp3s:
            mp3stat = mp3s[w]
        else:
            mp3stat = ph.descendant(w + ".mp3").real_stat()

        if mp3stat is not None:
            res.write(" <a href=\"%s.mp3\">.mp3</a>" % quote(w))
            res.write(" (" + sizeof_fmt(mp3stat.st_size) + ")")
        else:
            res.write(" <a class=\"convert\" href=\"%s.mp3/convert\">" % (
                quote(w)))
            res.write("Convert to mp3")
            res.write("</a>\n")
        res.write("</li>\n")
    res.write("</ul>")
    res.write("</body>")
    res.write("</html>")

    page = res.getvalue()
    res.close()

    start_response("200 OK", [("Content-Type", "text/html;charset=utf-8"),
                              ("Content-Length", str(len(page)))])
    return [page.encode("utf-8")]

def handle_file(environ, start_response, ph, real_file):
    content_type = {".wav": "audio/vnd.wave",
                    ".mp3": "audio/mpeg",
                    ".js": "application/javascript"}.get(ph.extension())
    if content_type is None:
        start_response("403 Forbidden", [])
        return []

    file = open(real_file.encode("utf-8"), "r")

    start_response("200 OK", [("Content-Type", content_type)])
    if 'wsgi.file_wrapper' in environ:
        return environ['wsgi.file_wrapper'](file, 1024 * 1024)
    else:
        return iter(lambda: file.read(1024 * 1024), '')
