$( document ).ready(function() {
    $(".convert").click(function() {
	var original_url = $(this).attr("href");

	var get_progress = function(url, context) {
	    $.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		context: context,
		success: progress,
	    });
	}

	var progress = function(json_data) {
	    if (json_data.status === "progress" || json_data.status === "enqueued")
	    {
		$(this).attr("value", json_data.progress);
		$(this).attr("max", json_data.workload);
		get_progress(original_url + "_cont/" + json_data.progress + "/" + json_data.workload, this);
	    }
	    else if (json_data.status === "done")
	    {
		var a = "<a href=\"" + json_data.link + "\">.mp3</a>";
		var sz = " (" + json_data.size + ")";
		$(a).replaceAll(this)
		    .css("background-color", "yellow")
		    .after(sz);
	    }
	}

	var progressbar = "<progress value='0' max='100'></progress>";

	get_progress(original_url + "_start",
		     $(progressbar).replaceAll(this))
	return false;
    })
});
