AUDIOROOT = /ljud
WSGIDIR = /etc/apache2/mp3wavweb

all: mp3wavweb.conf

mp3wavweb.conf: Makefile
	echo 'WSGIScriptAlias / $(WSGIDIR)/mp3wavweb.py' >> $@.tmp
	echo '<Directory $(WSGIDIR)>' >> $@.tmp
	echo '  Order allow,deny' >> $@.tmp
	echo '  Allow from all' >> $@.tmp
	echo '  AuthName "Audio Access"' >> $@.tmp
	echo '  AuthType Basic' >> $@.tmp
	echo '  AuthUserFile /etc/apache2/htpasswd.users' >> $@.tmp
	echo '  Require valid-user' >> $@.tmp
	echo '</Directory>' >> $@.tmp
	chmod 444 $@.tmp
	mv -f $@.tmp $@

install: all
	mkdir -p $(WSGIDIR)
	cp mp3wavweb.conf /etc/apache2/conf-available/
	a2enconf mp3wavweb
	echo "BASE = '$(AUDIOROOT)'" > $(WSGIDIR)/mp3wavcfg.py.tmp
	echo "socketpath = \"/tmp/mp3wavweb.sock\"" >> $(WSGIDIR)/mp3wavcfg.py.tmp
	mv -f $(WSGIDIR)/mp3wavcfg.py.tmp $(WSGIDIR)/mp3wavcfg.py
	cp mp3wavweb.py $(WSGIDIR)/
	cp wav2mp3d.py $(WSGIDIR)/
	mkdir -p $(WSGIDIR)/mp3wavweb/js
	cp mp3wavweb/js/jquery-2.1.0.min.js $(WSGIDIR)/mp3wavweb/js/
	cp mp3wavweb/js/mp3wavweb.js $(WSGIDIR)/mp3wavweb/js/
	service apache2 reload
