#!/usr/bin/env python3

import re
import os
import sys
import errno
import subprocess
import multiprocessing
import multiprocessing.connection

import mp3wavcfg

def listener_fileno(self):
    return self._listener._socket.fileno()

class Entry:
    def __init__(self, fn, workload):
        self.__fn = fn
        self.__recipients = {}
        self.__workload = workload

    def subscribe(self, client):
        self.__recipients[client] = True

    def send(self, verb, *args):
        to_remove = []
        for client, v in self.__recipients.items():
            try:
                client.send((verb, self.__fn) + tuple(args))
            except BrokenPipeError:
                to_remove.append(client)
        for client in to_remove:
            del self.__recipients[client]

    def fn(self):
        return self.__fn

    def progress_to_mb(self, progress):
        return (progress * self.__workload) // 100

    def handle_progress(self, progress, before_me):
        including_me = before_me + self.__workload
        self.send("progress", progress, including_me)
        return including_me

    def workload(self):
        return self.__workload

class Queue:
    def __init__(self):
        self.__queue = []
        self.__last_progres = 0

    def add(self, fn, workload):
        total_workload_before = -self.__last_progres
        for entry in self.__queue:
            total_workload_before += entry.workload()
            if entry.fn() == fn:
                return total_workload_before
        e = Entry(fn, workload)
        self.__queue.append(e)
        return total_workload_before + workload

    def subscribe(self, fn, client):
        workload = 0
        for entry in self.__queue:
            workload += entry.workload()
            if fn == entry.fn():
                entry.subscribe(client)
                return self.__last_progres, workload

    def send(self, verb, *args):
        self.__queue[0].send(verb, *args)

    def pop(self):
        self.__queue = self.__queue[1:]
        self.__last_progres = 0

    def empty(self):
        return len(self.__queue) == 0

    def files(self):
        return [e.fn() for e in self.__queue]

    def peek(self):
        if len(self.__queue) == 0:
            return None
        return self.__queue[0]

    def handle_progress(self, fn, progress):
        first = queue.peek()
        if first.fn() != fn:
            return

        self.__last_progres = first.progress_to_mb(progress)

        acc = 0
        for entry in self.__queue:
            acc = entry.handle_progress(self.__last_progres, acc)

queue = Queue()
proc = None

def work():
    global proc

    setattr(multiprocessing.connection.Listener, "fileno", listener_fileno)

    server = multiprocessing.connection.Listener(
        mp3wavcfg.socketpath, 'AF_UNIX', 3)

    clients = []

    while True:
        pending = [server] + clients
        if proc is not None:
            pending.append(proc.sentinel)
        ready = multiprocessing.connection.wait(pending)
        for sock in ready:
            if sock is server:
                clients.append(sock.accept())
            elif proc is not None and sock is proc.sentinel:
                proc.join()
                if proc.exitcode == 0:
                    queue.send("done")
                else:
                    queue.send("fail")
                proc = None
                queue.pop()
                if queue.peek() is not None:
                    queue.handle_progress(0, queue.peek().fn())
                start_encoding()
            else:
                try:
                    msg = sock.recv()
                except EOFError:
                    clients.remove(sock)
                else:
                    handle_message(sock, msg)

def real_filename(fn, cache):
    if cache:
        return os.path.join(mp3wavcfg.BASE, "mp3-cache", fn)
    else:
        return os.path.join(mp3wavcfg.BASE, fn)

def size_mb(fn, cache):
    try:
        status = os.stat(real_filename(fn, cache).encode("utf-8"))
    except FileNotFoundError:
        return None
    return status.st_size // (1024 * 1024)

def mp3_exists(fn):
    for cache in [True, False]:
        if size_mb(fn + ".mp3", cache) is not None:
            return True
    return False

def handle_message(client, msg):
    if msg[0] == "encode":
        fn = msg[1]
        sz = size_mb(fn + ".wav", False)
        if sz is None:
            client.send(("enofile", fn))
        elif mp3_exists(fn):
            client.send(("already-there", fn))
        else:
            workload = queue.add(fn, sz)
            client.send(("enqueued", fn, workload))
            start_encoding()
    elif msg[0] == "progress":
        # The "progress" message is sent from the encode() process.
        # It has two arguments: the file name (sans prefix and suffix,
        # and the progress in percent (0-100) as an integer.
        queue.handle_progress(msg[1], msg[2])
    elif msg[0] == "subscribe":
        fn = msg[1]
        res = queue.subscribe(fn, client)
        if res is None:
            if size_mb(fn + ".wav", False) is None:
                client.send(("enofile", fn))
            elif size_mb(fn + ".mp3", True) is None:
                client.send(("noqueue", fn))
            else:
                client.send(("done", fn))
    else:
        client.send(("badreq", msg[0]))

def start_encoding():
    global proc

    if proc is not None:
        return
    if queue.empty():
        return
    proc = multiprocessing.Process(target=encode, args=(queue.peek().fn(), ))
    proc.start()

def encode(fn):
    c = multiprocessing.connection.Client(mp3wavcfg.socketpath, 'AF_UNIX')
    result_file = real_filename(fn + ".mp3", True)
    result_dir = os.path.dirname(result_file)
    try:
        os.makedirs(result_dir.encode("utf-8"))
    except os.error as e:
        if e.errno != errno.EEXIST:
            raise

    cmd = ["lame", "--preset", "standard", "--nohist",
           real_filename(fn + ".wav", False).encode("utf-8"),
           (result_file + ".tmp").encode("utf-8")]
    lame = subprocess.Popen(args=cmd, bufsize=0, stdin=subprocess.DEVNULL,
                            stderr=subprocess.PIPE)
    percent_match = re.compile(".*\\(([0-9]+)%\\)")

    while True:
        msg = lame.stderr.read(100)
        if len(msg) == 0:
            break
        m = percent_match.search(str(msg))
        if m is not None:
            c.send(("progress", fn, int(m.group(1))))
    x = lame.wait()
    if x == 0:
        os.rename((result_file + ".tmp").encode("utf-8"),
                  result_file.encode("utf-8"))
    else:
        try:
            os.remove((result_file + ".tmp").encode("utf-8"))
        except OSError:
            pass
    sys.exit(x)

if __name__ == '__main__':
    work()
